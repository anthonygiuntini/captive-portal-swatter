#Captive Portal Swatter
#Script for Automatically logging into a captive portal (Specifically UD's)
#Anthony Giuntini

import urllib.request
import urllib.parse
import traceback
import time
from datetime import datetime, timedelta
import os.path
import sys
import webbrowser
import socket

#If DEBUG == True program will print exceptions and other bits of info
DEBUG = True

class PortalLogin:
    def __init__(self, username, password):
        self.USERNAME = username
        self.PASSWORD = password
        self.RECURSION_MAX = 4 #number of times to repeat before giving up
        self.recursion_count = 0
        self.running = True
        self.CHECK_EVERY = 5 #seconds
        self.next_checktime = time.time()
        self.TEST_PAGEURL = 'https://www.google.com'
        self.run()
    
    def run(self):
        #Contains main loop
        
        while self.running:
            if DEBUG: print('next loop')
            
            if self.time2check():
            
                connected, actual_url = self.check_connection()
                if DEBUG: print('Actual url: %s' % actual_url)
                if connected:
                    #if the test url is reached log success to file
                    if DEBUG: print('Connected to %s' % actual_url)
                    self.log_to_file(logtype='record', content='Connection confirmed')
                else:
                    #if not attempt to login
                    if DEBUG: print('Not connected')
                    try:
                        successful = self.login(actual_url)

                        if successful:
                            if DEBUG: print('\033[92m Login Successful \033[0m')
                            self.log_to_file(logtype='record', content='Login Successful')
                            self.recursion_count = 0
                        
                        elif self.recursion_count < self.RECURSION_MAX:
                            #if not successful try every 5 seconds for set number of times
                            if DEBUG: print('Trying again, Recursion: %d' % self.recursion_count)
                            self.add_time(time.time(), 5)
                            self.recursion_count += 1

                        else:
                            try:
                                #Checks to see if there is a network
                                test_sock = socket.socket()
                                test_sock.connect(('8.8.8.8',53))
                                test_sock.close()
                                if DEBUG: print('Gave up on connection')

                                #Opens Browser if it could not reach site but there is a network
                                webbrowser.open(self.TEST_PAGEURL)
                                self.recursion_count = 0
                                self.next_checktime = self.add_time(time.time(), 10)
                            except OSError as exception:
                                #If no network check again every 10s
                                if str(exception) == '[Errno 51] Network is unreachable':
                                    if DEBUG: print('No Network')
                                    self.next_checktime = self.add_time(time.time(),10)

                    except:
                        if DEBUG: traceback.print_exc(file=sys.stdout)
                        self.log_to_file()
                
            time.sleep(1) #Stops the program in between checks for given amount
            
    def check_connection(self):
        #Checks to see if the desired url is the actual url
        try:
            test_url = urllib.request.urlopen(self.TEST_PAGEURL)
            actual_url = test_url.geturl()
            
            if  actual_url == self.TEST_PAGEURL:
                self.next_checktime = self.add_time(time.time(), self.CHECK_EVERY)
                return True, actual_url
            else:
                return False,actual_url

        except (urllib.error.URLError, ConnectionResetError):
            if DEBUG: print('UrlError')
            return False, self.TEST_PAGEURL#actual_url

    def login(self, portalurl):
        #Inserts user's username and password
        try:
            server = portalurl
            parems = urllib.parse.urlencode({'user':self.USERNAME,'password':self.PASSWORD})
            parems = parems.encode('ascii')
            site_content = urllib.request.urlopen(portalurl, parems)
            return True
        except urllib.error.URLError:
            return False
            

    def add_time(self, current_time, amount):
        #Adds the specified amount of time to the given time
        return current_time + amount

    def time2check(self):
        #Checks to see if it is time to check connection
        time_change = self.next_checktime - time.time()
        if DEBUG: print('time change: %d' % time_change)
        if time_change > 0:
            return False
        elif time_change <= 0:
            return True
        else:
            return None

    def log_to_file(self, logtype='traceback',content='N/A'):
        #Writes a record or exception traceback to respective files
        y,mn,d,h,m,s,a,b,c = time.localtime()
        current_dir = self.get_current_dir()
        if DEBUG: print('current dir: %s' % current_dir)
        if logtype == 'traceback':
            self.log_to_file(logtype='record', content='exception')
            with open('%s/logfiles/tb.txt' % current_dir,'a') as file:                
                file.write('===================%d/%d/%d %d:%d:%d=====================\n'\
                           % (mn,d,y,h,m,s))
                traceback.print_exc(file=file)
                file.write('\n')
        elif logtype == 'record':
            with open('%s/logfiles/record.txt' % current_dir,'a') as file:
                file.write('%s @ %d/%d/%d %d:%d:%d\n' % (content, mn,d,y,h,m,s))
                file.write('\n')

    def get_current_dir(self):
        #This avoids an error with cx_freeze where __file__ is not known
        if getattr(sys, 'frozen', False):
            #Frozen
            current_directory = os.path.dirname(sys.executable)
        else:
            #Not frozen
            current_directory = os.path.dirname(os.path.realpath(__file__))
        return current_directory
        

if __name__ == '__main__':
    pl = PortalLogin('agiuntini', '900860702')
